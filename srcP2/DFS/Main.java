
package DFS;

import java.io.*;
import java.util.*;
import graph.Graph;
import graph.Node;

public class Main {

	static Graph g = new Graph(Graph.GraphType.UNDIRECTED);
	static int sol;
	static int ok;

	final static String PATH = "portal.in";

	public static void main(String... args) throws IOException{

		/*open output file*/
		Scanner scanner = new Scanner(new File(PATH));
		File file = new File("portal.out");
		BufferedWriter output = null;
		output = new BufferedWriter(new FileWriter(file));
		/*read data from input file*/
		int test_count = 1;
		for (int i = 1; i <= test_count; ++i){
			g.readData(scanner);
		}
		solve();
		/*write solution to output file*/
		output.write(sol + "");
		/*close files*/
		output.close();
		scanner.close();
	}

	public static void dfs(Node nod, Node parent){
		nod.visited = 1;
		for(Node n : g.getEdges(nod)){
			if(!n.wasVisited()){
				dfs(n,nod);
			}
			else
				/*check if the connected component has cycle*/
				if(ok == 0 && n != parent){
					sol--;
					ok = 1;
				}
		}
	}

	public static void solve(){
		/*create pseudo-parent for root node*/
		Node initParent = new Node("", -1);
		/*DFS from each node to find connected components*/
		for(Node n : g.getNodes()){
			if(!n.wasVisited()){
				sol++;
				ok = 0;
				dfs(n, initParent);
			}
		}
	}
}
