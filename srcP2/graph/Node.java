
package graph;

public class Node {

	String _name;
	int _id;

	public Node(String name, int id) {
		_name = name;
		_id = id;
		reset();
	}

	public Node(int id) {
		_name = "";
		_id = id;
		reset();
	}

	public int visited;

	public final static int UNSET = -1;
	
	/**
	 * @return true if the node was visited, false otherwise.
	 */
	public boolean wasVisited() {
		return !(visited == UNSET);
	}

	/**
	 * Resets the state of the node.
	 */
	public void reset() {
		visited = UNSET;
	}

	public String getName() {
		return _name;
	}

	public int getId() {
		return _id;
	}

	@Override
	public String toString() {

		StringBuilder ans = new StringBuilder();
		ans.append("Node : ");

		if (_name.length() != 0) {
			ans.append(_name);
			ans.append(" , ");
		}

		ans.append(_id);
		return ans.toString();
	}

}
