
package graph;

import java.util.ArrayList;
import java.util.Scanner;

public class Graph {

	public enum GraphType {
		DIRECTED, UNDIRECTED
	};

	public GraphType _type;

	ArrayList<Node> nodes;
	ArrayList<ArrayList<Node>> edges;

	public Graph(GraphType type) {
		nodes = new ArrayList<Node>();
		edges = new ArrayList<ArrayList<Node>>();
		_type = type;
	}

	/**
	 * @return number of nodes in the graph
	 */
	public int getNodeCount() {
		return nodes.size();
	}
	public void insertEdge(Node node1, Node node2) {
		edges.get(node1.getId()).add(node2);
	}

	/**
	 * Adds the argument node to the graph.
	 * 
	 * @param node
	 */
	public void insertNode(Node node) {
		nodes.add(node);
		edges.add(new ArrayList<Node>());
	}

	/**
	 * @return a list with all the nodes in the graph
	 */
	public ArrayList<Node> getNodes() {
		return nodes;
	}

	/**
	 * @param node
	 * @return a list with all the neighbors of the given node
	 */
	public ArrayList<Node> getEdges(Node node) {
		return edges.get(node.getId());
	}

	/**
	 * This method removes all the temporary information related to the state
	 * of the graph during traversals. (for example it sets every node as being
	 * unvisited)
	 */
	public void reset() {

		for (Node n : nodes)
			n.reset();
	}

	public void readData(Scanner scanner) {

		if (scanner == null)
			return;

		int numNodes = scanner.nextInt();
		int numEdges = scanner.nextInt();

		for (int i = 0; i < numNodes; ++i) {
			Node new_node = new Node(i);
			insertNode(new_node);
		}

		for (int i = 0; i < numEdges; ++i) {
			int node1 = scanner.nextInt() - 1;
			int node2 = scanner.nextInt() - 1;
			if (_type == Graph.GraphType.UNDIRECTED) {
				insertEdge(nodes.get(node2), nodes.get(node1));
				insertEdge(nodes.get(node1), nodes.get(node2));
			}
		}

	}

	@Override
	public String toString() {
		StringBuilder ans = new StringBuilder();

		ans.append("Graph:\n");
		for (Node n : nodes) {
			ans.append(n.toString() + " : ");
			ans.append(edges.get(n.getId()));
			ans.append('\n');
		}
		ans.append('\n');
		return ans.toString();
	}
}
