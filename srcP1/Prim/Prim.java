package Prim;

import graph.Edge;
import graph.Graph;
import graph.Pair;

import java.util.*;

public class Prim {
	
	private int[] d;
	private Graph g;
	public long totalCost;

	public Prim(Graph g) {
		this.g = g;
		d = new int[g.getNodeCount()];
	}

	private class NodeComparator implements Comparator<Pair<Integer,Integer>> {
		/**
		 * Compares nodes using the current estimation of the distance from
		 * the source node.
		 */
		@Override
		public int compare(Pair<Integer,Integer> arg0, Pair<Integer,Integer> arg1) {
			int dist1 = arg0.second();
			int dist2 = arg1.second();

			return dist1 > dist2 ? 1 : -1;
		}
	}

	/*function that prints the priority queue
	 * used for debugging*/
	public void printQueue(PriorityQueue<Pair<Integer,Integer>> pq){
		/*create iterator from the queue*/
		java.util.Iterator<Pair<Integer, Integer>> it = pq.iterator();

		/*prints the queue*/
		System.out.println ( "Priority queue values are: ");
		while (it.hasNext()){
			Pair<Integer,Integer> pair = it.next();
			System.out.println ( "Value: "+ pair.first() + " distance: " + pair.second()); 
		}
	}
	/*function that prints the list of edges in MST
	 * used for debugging*/
	public void printMST(List<Pair<Integer, Integer>> edgesMST){
		for(Pair<Integer,Integer> node: edgesMST)
			System.out.println(node.first() + " -> " + node.second());
	}
	/*function that prints the distances vector
	 * used for debugging
	 */
	public void printD(){
		for(int i = 1; i < g.getNodeCount(); i++){
			System.out.print(d[i] + " ");
		}
		System.out.println();
	}

	public List<Pair<Integer, Integer>> PrimAlg(Graph g, int root, Edge e){

		List<Pair<Integer, Integer>> edgesMST = new ArrayList<Pair<Integer, Integer>>();
		PriorityQueue<Pair<Integer,Integer>> pq = 
			new PriorityQueue<>(g.getNodeCount(),new NodeComparator());
		int[] d = new int[g.getNodeCount()];
		int[] p = new int[g.getNodeCount()];

		/*initialize the distances and parent vectors*/
		for(int u = 1; u < g.getNodeCount(); u++) {
			d[u] = Integer.MAX_VALUE;
			p[u] = -1;
		}

		d[root] = 0;

		/*if there is an alternate path
		 * add the given edge from the start*/
		if(e != null){
			edgesMST.add(new Pair<Integer,Integer>(e.nod1,e.nod2));

			/*add the the neighbors of both ends of the edge*/
			for (Pair<Integer, Integer> v : g.getEdges(e.nod1)) {
				if(v.second() < d[v.first()]){
					d[v.first()] = v.second();
					p[v.first()] = e.nod1;
				}
			}

			for (Pair<Integer, Integer> v : g.getEdges(e.nod2)) {
				if(v.second() < d[v.first()]){
					d[v.first()] = v.second();
					p[v.first()] = e.nod2;
				}
			}
			d[e.nod2] = e.cost;

			/*add the rest of the nodes to the priority queue*/
			for(int i = 1; i < g.getNodeCount(); i++)
				if(i != e.nod1 && i != e.nod2)
					pq.add(new Pair<Integer, Integer>(i,d[i]));
		}
		/*if there is no given edge,
		 * just add the nodes to the priority queue*/
		else{
			for(int i = 1; i < g.getNodeCount(); i++)
				pq.add(new Pair<Integer, Integer>(i,d[i]));
		}

		while(!pq.isEmpty()){
			/*extract node with shortest distance*/
			Pair<Integer,Integer> node = pq.poll();
			int u = node.first();

			/*add it to MST*/
			edgesMST.add(new Pair<Integer,Integer>(u,p[u]));

			/*add the neighbors to the priority queue*/
			for (Pair<Integer, Integer> v : g.getEdges(u)) {				
				if(v.second() < d[v.first()]){
					if(pq.remove(new Pair<Integer,Integer>(v.first(),d[v.first()]))){
						d[v.first()] = v.second();
						p[v.first()] = u;
						pq.add(new Pair<Integer,Integer>(v.first(),d[v.first()]));
					}
				}
			}
		}
		edgesMST.remove(new Pair<Integer,Integer>(root,p[root]));

		/*compute the total cost of MST*/
		for(int i = 1; i < g.getNodeCount(); i++){
			totalCost += d[i];
		}
		return edgesMST;
	}
}