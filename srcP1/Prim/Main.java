
package Prim;

import java.io.*;
import java.util.*;

import graph.Graph;
import graph.Pair;


public class Main {

	final static String PATH = "kim.in";

	public static void main(String ... args) throws IOException {
		Scanner sc = new Scanner(new File(PATH));
		String strLong;
		BufferedWriter output = null;

		/*create the graph and read the data*/
		Graph g = new Graph();
		g.readData(sc);

		try {
			/*open output file*/
			File file = new File("kim.out");
			output = new BufferedWriter(new FileWriter(file));

			Prim solver = new Prim(g);

			@SuppressWarnings("unused")
			List<Pair<Integer, Integer>> muchiiAMA = solver.PrimAlg(g, 1, null );
			strLong = Long.toString(solver.totalCost);
			/*write total cost of AMA in output file*/
			output.write(strLong+"\n");
			solver.totalCost = 0;

			/*apply Prim's algorithm for every alternate path*/
			for(int i = 1; i <= g.numAlternatePath; i++){
				muchiiAMA = solver.PrimAlg(g, g.edgesList.get(g.alternatePath[i] - 1).nod1,
						g.edgesList.get(g.alternatePath[i] - 1));
				/*write cost of every alternate path in output file*/
				strLong = Long.toString(solver.totalCost);				
				output.write(strLong+"\n");
				/*reset total cost*/
				solver.totalCost = 0;
			}
		} catch ( IOException e ) {
			e.printStackTrace();
		} finally {
			if ( output != null ) {
				output.close();          
			}
			sc.close();
		}		
	}
}
