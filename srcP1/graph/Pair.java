
package graph;

public class Pair<A, B> {

	private A fst;

	private B snd;

	public Pair(A fst, B snd) {
		this.fst = fst;
		this.snd = snd;
	}

	public A first() {
		return fst;
	}

	public B second() {
		return snd;
	}
	@Override
	public boolean equals(Object o)
	{
	    if (o instanceof Pair) {
	       @SuppressWarnings("unchecked")
		Pair<A, B> pair = (Pair<A, B>) o;
	       return fst.equals(pair.first()) && snd.equals(pair.second());
	    }
	    return false;
	}

}