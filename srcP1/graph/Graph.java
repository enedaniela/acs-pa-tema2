package graph;

import java.util.*;

public class Graph {

	/**
	 * Total number of nodes that makes the graph
	 */
	private int numNodes;
	public int numAlternatePath;

	/**
	 * The graph uses list of adjacency for each node.
	 * The first item of the pair represents the index of the adjacent,
	 * while the second item represents the cost of the edge
	 */
	private List<List<Pair<Integer, Integer>>> edges;
	public List<Edge> edgesList;
	public int[] alternatePath;

	public Graph() {
		edges = new ArrayList<>();
	}

	public void insertNode(int nodeIdx) {
		edges.add(new ArrayList<Pair<Integer, Integer>>());
		
	}

	/**
	 * Inserts a new edge into the graph starting at 'fromNodeIdx' and
	 * ending at 'toNodeIdx', having cost value of 'cost'
	 *
	 * @param fromNodeIdx
	 * @param toNodeIdx
	 * @param cost
	 */
	public void insertEdge(int fromNodeIdx, int toNodeIdx, int cost) {
		edges.get(fromNodeIdx).add(new Pair<>(toNodeIdx, cost));
	}

	public int getNodeCount() {
		return numNodes + 1;
	}

	public List<Pair<Integer, Integer>> getEdges(int nodeIdx) {
		return edges.get(nodeIdx);
	}

	public void readData(Scanner scanner) {

		numNodes = scanner.nextInt();
		int numEdges = scanner.nextInt();		
		numAlternatePath = scanner.nextInt();
		alternatePath = new int[numAlternatePath + 1];
		edgesList = new ArrayList<>();

		for (int i = 1; i <= numNodes + 1; i++) {
			insertNode(i);
		}

		for (int edgeIdx = 0; edgeIdx < numEdges; edgeIdx++) {
			int node1 = scanner.nextInt();
			int node2 = scanner.nextInt();
			int cost = scanner.nextInt();
			edgesList.add(new Edge(node1, node2, cost));
			insertEdge(node1, node2, cost);
			insertEdge(node2, node1, cost);
		}
		
		for(int altIdx = 1; altIdx <= numAlternatePath; altIdx++)
			alternatePath[altIdx] = scanner.nextInt();
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder("Print Graph:\n");

		for(int n = 0; n < numNodes; n++) {
			sb.append("OutEdges for " + n + " -> ");

			for (Pair<Integer, Integer> edge : edges.get(n)) {
				sb.append(edge.first());
                sb.append("( " + edge.second() + " ) | ");
			}

			sb.append("\n");
		}
		sb.append("\n");

		return sb.toString();
	}
}