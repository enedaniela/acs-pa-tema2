package graph;

public class Edge {
	public int nod1;
	public int nod2;
	public int cost;
	
	public Edge(int nod1, int nod2, int cost){
		this.nod1 = nod1;
		this.nod2 = nod2;
		this.cost = cost;
	}
	
}
