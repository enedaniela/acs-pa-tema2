.PHONY: build clean run

build:
	ls classes || mkdir classes;
	javac srcP1/graph/*.java -d classes;
	javac -cp classes srcP1/Prim/*.java -d classes;
	ls classes1 || mkdir classes1;
	javac srcP2/graph/*.java -d classes1;
	javac -cp classes1 srcP2/DFS/*.java -d classes1;
run-p1:
	java -cp classes Prim/Main
run-p2:
	java -Xss18m -cp classes1 DFS/Main
clean:
	rm -r classes
	rm -r classes1
